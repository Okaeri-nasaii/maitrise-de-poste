# Footprinting

# I - SelfFootPrinting 

## Host OS
* Nom de la machine : 
    * Commande : `hostname`
    * Name : ASUS-Sabine
* OS et version : 
    * Commande : `Get-WMIObject win32_OperatingSystem Caption, Version, OSArchitecture`
    * Name : Microsoft Windows 10 Famille || 10.0.19041 N/A version 19041
* Architecture Processeur : 
    * Commande : `wmic os get OSArchitecture`
    * Processeur : 64 bits
* Quantité de RAM et Modèle : 
    * Commande : `Get-CimInstance win32_physicalmemory`
    * Quantité de RAM : 4294967296
    * Modèle : 8KTF51264HZ-1G6N1

## Devices
* Processeur :
    * Commande : `Get-WMIObject win32_Processor`
    * Name : Intel(R) Core(TM) i3-5005U CPU @ 2.00GHz
        * `i3` est la famille du processeur
        * le `5` représente la génération du processeur
        * `005` est le numéro du processeur
        * le `U` est une nomenclature qui donne des indices sur la performance et la consommation de l'ordinateur    
* Marque et Modèle :
    * Touchpad | Trackpad :
        * Commande : `Get-WmiObject win32_PointingDevice`
        * Name : ASUS TrackPad
    * Carte Graphique : 
        * Commande : `Get-WMIObject win32_VideoController`
        * Name : NVIDIA GeForce 920M
* Disque dur : 
    * Nom et Modèle : 
        * Commande : `Get-WMIObject win32_diskdrive`
            * Disque 1 : 
                Model 1 : SAMSUNG MZ7LF128HCHP-00004
            * Disque 2 : 
                Modèle 2 : TOSHIBA MQ01ABD100
    * Partitions : 
        * Commande : `Get-Partition | fl`
            * Disque 1 :
                Partition 1 : Offset : 1048576 | Size : 260 MB | Type : System
                Partition 2 : Offset : 273678336 | Size : 16 MB | Type : Restricted 
                Partition 3 : Offset : 290455552 | Size : 118.48 GB | Type : Basic
                Partition 4 : Offset : 127512084480 | Size : 499 MB | Type : Recovery
            * Disque 2 : 
                Partition 1 : Offset : 1048576 | Size : 931.51 GB | Type : Basic 
        
## Users
* Liste complête des utilisateurs : 
    * Commande : `wmic useraccount list full`
        * Compte 1 : Administrator
        * Compte 2 : DefaultAccount
        * Compte 3 : Invité
        * Compte 4 : Macbook
        * Compte 5 : Milh Sabine
        * Compte 6 : WDAGUtilityAccount
    Le premier compte est celui en "Full Admin"

## Processus

* Commande : `ps`
    Handles | NPM(K) | PM(K) | WS(K) | CPU(s) | Id | SI | ProcessName
     4578   |  0     | 200   | 132   |        | 4  | 0  | System
     2468   |  84    | 56580 | 49968 | 328.95 |6652| 1  | explorer
     747    |  24    | 1836  | 1096  |        |600 | 0  | csrss
     276    |  12    | 2736  | 2648  |        |1032| 1  | winlogon
     1589   |  31    | 8976 | 11044  |        |788 | 0  | lsass
     * System : 




## Network

* Cartes Réseaux :
    * Commande : `Get-NetAdapter | fl name`
    * Name : 
        - Wi-Fi
        - Ethernet
        - Ethernet 2

# 2 - Scripting
Voir Script1.ps1 et Script2.ps1

# 3 - Gestion des Softs
un gestionnaire de paquets se connecte à des dépôts en ligne dans lesquels sont stockés les logiciels sous forme de paquets.L'utiliser permet de savoir si le programme est signé et donc si l'origine peut être fiable; les logiciels également sont dépourvus de spywares, malwares car ils se trouvent sur un dépôt/serveur qui va vérifier l'intégrité des logiciels déposés. c'est mieux parce-que certains logiciels d'internet ralentissent le pc en utilisant sa puissance de calcul ou/et peuvent espionner des données confidentielles

* Liste de tous les paquets :
    * Commande : `choco list --local-only`
        Chocolatey v0.10.15
        chocolatey 0.10.15
        chocolatey-core.extension 1.3.5.1
        chocolatey-dotnetfx.extension 1.0.1
        chocolatey-visualstudio.extension 1.8.1
        chocolatey-windowsupdate.extension 1.0.4
        dotnetfx 4.8.0.20190930
        KB2919355 1.0.20160915
        KB2919442 1.0.20160915
        KB2999226 1.0.20181019
        KB3033929 1.0.5
        KB3035131 1.0.3
        python 3.8.3
        python3 3.8.3
        vcredist140 14.26.28720.3
        vcredist2015 14.0.24215.20170201
        visualstudio-installer 2.0.1
        visualstudio2017buildtools 15.9.23.0
        17 packages installed.


* Origine des paquets : 
 Chocolatey v0.10.15
 chocolatey 0.10.15
 Title: Chocolatey | Published: 11/02/2020
 Number of Downloads: n/a | Downloads for this version: n/a
 Package url
 Chocolatey Package Source: n/a
 Tags: nuget apt-get machine repository chocolatey
 Software Site: https://github.com/chocolatey/choco
 Software License: https://raw.githubusercontent.com/chocolatey/choco/master/LICENSE
 Summary: Chocolatey is the package manager for Windows (like apt-get but for Windows)
 Description: Chocolatey is a package manager for Windows (like apt-get but for Windows). It was designed to be a decentralized framework for quickly installing applications and tools that you need. It is built on the NuGet infrastructure currently using PowerShell as its focus for delivering packages from the distros to your door, err computer.

  Chocolatey is brought to you by the work and inspiration of the community, the work and thankless nights of the [Chocolatey Team](https://github.com/orgs/chocolatey/people), with Rob heading up the direction.

  You can host your own sources and add them to Chocolatey, you can extend Chocolatey's capabilities, and folks, it's only going to get better.

  ### Information

   * [Chocolatey Website and Community Package Repository](https://chocolatey.org)
   * [Mailing List](http://groups.google.com/group/chocolatey) / [Release Announcements Only Mailing List](https://groups.google.com/group/chocolatey-announce) / [Build Status Mailing List](http://groups.google.com/group/chocolatey-build-status)
   * [Twitter](https://twitter.com/chocolateynuget) / [Facebook](https://www.facebook.com/ChocolateySoftware) / [Github](https://github.com/chocolatey)
   * [Blog](https://chocolatey.org/blog) / [Newsletter](https://chocolatey.us8.list-manage1.com/subscribe?u=86a6d80146a0da7f2223712e4&id=73b018498d)
   * [Documentation](https://chocolatey.org/docs) / [Support](https://chocolatey.org/support)

  ### Commands
  There are quite a few commands you can call - you should check out the [command reference](https://chocolatey.org/docs/commands-reference). Here are the most common:

   * Help - choco -? or choco command -?
   * Search - choco search something
   * List - choco list -lo
   * Config - choco config list
   * Install - choco install baretail
   * Pin - choco pin windirstat
   * Outdated - choco outdated
   * Upgrade - choco upgrade baretail
   * Uninstall - choco uninstall baretail

  #### Alternative installation sources:
   * Install ruby gem - choco install compass -source ruby
   * Install python egg - choco install sphynx -source python
   * Install windows feature - choco install IIS -source windowsfeatures
   * Install webpi feature - choco install IIS7.5Express -source webpi

  #### More
  For more advanced commands and switches, use `choco -?` or `choco command -h`. You can also look at the [command reference](https://chocolatey.org/docs/commands-reference), including how you can force a package to install the x86 version of a package.

  ### Create Packages?
  We have some great guidance on how to do that. Where? I'll give you a hint, it rhymes with socks! [Docs!](https://chocolatey.org/docs/create-packages)

  In that mess there is a link to the [PowerShell Chocolatey module reference](https://chocolatey.org/docs/helpers-reference).
 Release Notes: See all - https://github.com/chocolatey/choco/blob/stable/CHANGELOG.md

chocolatey-core.extension 1.3.5.1
 Title: Chocolatey Core Extensions | Published: 06/07/2020
 Number of Downloads: n/a | Downloads for this version: n/a
 Package url
 Chocolatey Package Source: https://github.com/chocolatey/chocolatey-coreteampackages/tree/master/extensions/chocolatey-core.extension
 Tags: chocolatey core extension admin
 Software Site: https://github.com/chocolatey/chocolatey-coreteampackages
 Software License: https://github.com/chocolatey/chocolatey-coreteampackages/blob/master/LICENSE.md
 Software Source: https://github.com/chocolatey/chocolatey-coreteampackages
 Documentation: https://github.com/chocolatey/chocolatey-coreteampackages/tree/master/extensions/chocolatey-core.extension/README.md
 Issues: https://github.com/chocolatey/chocolatey-coreteampackages/issues
 Summary: Helper functions extending core choco functionality
 Description: This package provides helper functions installed as a Chocolatey extension.
  These functions may be used in Chocolatey install/uninstall scripts by declaring this package a dependency in your package's nuspec.
 Release Notes: https://github.com/chocolatey/chocolatey-coreteampackages/tree/master/extensions/chocolatey-core.extension/CHANGELOG.md

chocolatey-dotnetfx.extension 1.0.1
 Title: Chocolatey .NET Framework extensions | Published: 06/07/2020
 Number of Downloads: n/a | Downloads for this version: n/a
 Package url
 Chocolatey Package Source: https://github.com/jberezanski/ChocolateyPackages/tree/master/chocolatey-dotnetfx.extension
 Tags: chocolatey extension admin windows update .net net dotnet framework devpack developer pack
 Software Site: https://github.com/jberezanski/ChocolateyPackages/tree/master/chocolatey-dotnetfx.extension
 Software License: https://cdn.rawgit.com/jberezanski/ChocolateyPackages/8086c84fed16d4150a50ba1e97fd6b75e3c4f511/LICENSE
 Documentation: https://github.com/jberezanski/ChocolateyPackages/tree/master/chocolatey-dotnetfx.extension/README.md
 Issues: https://github.com/jberezanski/ChocolateyPackages/issues
 Summary: Helper functions useful for developing packages for Microsoft .NET Framework runtime and Developer Pack.
 Description: This package provides helper functions useful for developing packages for Microsoft .NET Framework runtime and Developer Pack.
 Release Notes: [Change log](https://github.com/jberezanski/ChocolateyPackages/tree/master/chocolatey-dotnetfx.extension/CHANGELOG.md)

chocolatey-visualstudio.extension 1.8.1
 Title: Chocolatey Visual Studio servicing extensions | Published: 06/07/2020
 Number of Downloads: n/a | Downloads for this version: n/a
 Package url
 Chocolatey Package Source: https://github.com/jberezanski/ChocolateyPackages/tree/master/chocolatey-visualstudio.extension
 Tags: chocolatey extension admin visual studio
 Software Site: https://github.com/jberezanski/ChocolateyPackages/tree/master/chocolatey-visualstudio.extension
 Software License: https://cdn.rawgit.com/jberezanski/ChocolateyPackages/8086c84fed16d4150a50ba1e97fd6b75e3c4f511/LICENSE
 Documentation: https://github.com/jberezanski/ChocolateyPackages/tree/master/chocolatey-visualstudio.extension/README.md
 Issues: https://github.com/jberezanski/ChocolateyPackages/issues
 Summary: Helper functions useful for developing packages for installing and servicing Microsoft Visual Studio.
 Description: This package provides helper functions useful for developing packages for installing and servicing Microsoft Visual Studio.
 Release Notes: [Change log](https://github.com/jberezanski/ChocolateyPackages/tree/master/chocolatey-visualstudio.extension/CHANGELOG.md)

chocolatey-windowsupdate.extension 1.0.4
 Title: Chocolatey Windows Update extensions | Published: 06/07/2020
 Number of Downloads: n/a | Downloads for this version: n/a
 Package url
 Chocolatey Package Source: https://github.com/jberezanski/ChocolateyPackages/tree/master/chocolatey-windowsupdate.extension
 Tags: chocolatey extension admin windows update kb
 Software Site: https://github.com/jberezanski/ChocolateyPackages/tree/master/chocolatey-windowsupdate.extension
 Software License: https://cdn.rawgit.com/jberezanski/ChocolateyPackages/8086c84fed16d4150a50ba1e97fd6b75e3c4f511/LICENSE
 Documentation: https://github.com/jberezanski/ChocolateyPackages/tree/master/chocolatey-windowsupdate.extension/README.md
 Issues: https://github.com/jberezanski/ChocolateyPackages/issues
 Summary: Helper functions useful for developing packages for Windows updates (KBs).
 Description: This package provides helper functions useful for developing packages for Windows updates (KBs).
 Release Notes: [Change log](https://github.com/jberezanski/ChocolateyPackages/tree/master/chocolatey-windowsupdate.extension/CHANGELOG.md)

dotnetfx 4.8.0.20190930
 Title: Microsoft .NET Framework 4.8 | Published: 06/07/2020
 Number of Downloads: n/a | Downloads for this version: n/a
 Package url
 Chocolatey Package Source: https://github.com/jberezanski/ChocolateyPackages/tree/master/dotnetfx
 Tags: microsoft .net framework dotnet desktop clr redistributable runtime admin
 Software Site: https://www.microsoft.com/net/
 Software License: http://msdn.microsoft.com/en-US/cc300389.aspx
 Documentation: https://docs.microsoft.com/en-us/dotnet/framework/
 Issues: https://github.com/Microsoft/dotnet/issues
 Summary: The .NET Framework is a development platform for building apps for web, Windows, Windows Phone, Windows Server, and Microsoft Azure.
 Description: The .NET Framework is a development platform for building apps for web, Windows, Windows Phone, Windows Server, and Microsoft Azure. It consists of the common language runtime (CLR) and the .NET Framework class library, which includes a broad range of functionality and support for many industry standards.

  The .NET Framework provides many services, including memory management, type and memory safety, security, networking, and application deployment. It provides easy-to-use data structures and APIs that abstract the lower-level Windows operating system. You can use a variety of programming languages with the .NET Framework, including C#, F#, and Visual Basic.

  Supported Windows Client versions: Windows 10 version 1903, Windows 10 version 1809, Windows 10 version 1803, Windows 10 version 1709, Windows 10 version 1703, Windows 10 version 1607, Windows 8.1, Windows 7 SP1

  Supported Windows Server versions: Windows Server 2019, Windows Server version 1803, Windows Server 2016, Windows Server 2012 R2, Windows Server 2012, Windows Server 2008 R2 SP1

  The matching Developer Pack can be installed using [this package](https://chocolatey.org/packages/netfx-4.8-devpack).
 Release Notes: ##### Software
  [.NET Framework 4.8 announcement](https://devblogs.microsoft.com/dotnet/announcing-the-net-framework-4-8/)
  [.NET Framework 4.8 release notes](https://github.com/Microsoft/dotnet/blob/master/releases/net48/README.md)
  [.NET Framework 4.8 changes](https://github.com/Microsoft/dotnet/blob/master/releases/net48/dotnet48-changes.md)
  ##### Package
  4.8.0.20190930: Updated extension dependency.

KB2919355 1.0.20160915
 Title: Windows RT 8.1, Windows 8.1, and Windows Server 2012 R2 update: April 2014 (KB2919355) | Published: 06/07/2020
 Number of Downloads: n/a | Downloads for this version: n/a
 Package url
 Chocolatey Package Source: n/a
 Tags: Microsoft Visual Studio 2015 Windows Update KB2919355
 Software Site: https://support.microsoft.com/en-us/kb/2919355
 Software License: https://support.microsoft.com/en-us/kb/2919355
 Summary: This update is a cumulative update that includes the security updates and the non-security updates for Windows RT 8.1, Windows 8.1, and Windows Server 2012 R2 that were released before March 2014.
 Description: This update is a cumulative update that includes the security updates and the non-security updates for Windows RT 8.1, Windows 8.1, and Windows Server 2012 R2 that were released before March 2014. In addition to previous updates, it includes features such as improved Internet Explorer 11 compatibility for enterprise applications, usability improvements, extended mobile device management, and improved hardware support.

  All future security and nonsecurity updates for Windows RT 8.1, Windows 8.1, and Windows Server 2012 R2 require this update to be installed.

  On systems other than Windows 8.1 or Windows Server 2012 R2, this package installs successfully, but does nothing.

  This update is required to install Visual Studio 2015 on Windows 8.1 or Windows Server 2012 R2.
 Release Notes: 1.0.20160915:
  - fixed installation on PowerShell 2.0

KB2919442 1.0.20160915
 Title: March 2014 servicing stack update for Windows 8.1 and Windows Server 2012 R2 (KB2919442) | Published: 06/07/2020
 Number of Downloads: n/a | Downloads for this version: n/a
 Package url
 Chocolatey Package Source: n/a
 Tags: Microsoft Visual Studio 2015 Windows Update 2919442
 Software Site: https://support.microsoft.com/en-us/kb/2919442
 Software License: https://support.microsoft.com/en-us/kb/2919442
 Summary: The update fixes some issues in the servicing stack of Windows 8.1 and Windows Server 2012 R2.
 Description: The servicing stack includes the files and resources that are required to service a Windows image. This includes the Package Manager executable, the required servicing libraries, and other resources. The servicing stack is included in all Windows installations. The update fixes some issues.

  On systems other than Windows 8.1 or Windows Server 2012 R2, this package installs successfully, but does nothing.

  This update is required to install Visual Studio 2015 on Windows 8.1 or Windows Server 2012 R2.
 Release Notes: 1.0.20160915:
  - fixed installation on PowerShell 2.0
  - fixed installation on machines which already have a superseding hotfix installed

KB2999226 1.0.20181019
 Title: Update for Universal C Runtime in Windows (KB2999226) | Published: 06/07/2020
 Number of Downloads: n/a | Downloads for this version: n/a
 Package url
 Chocolatey Package Source: n/a
 Tags: microsoft visual studio 2015 windows update kb2919355
 Software Site: https://support.microsoft.com/en-us/kb/2999226
 Software License: https://support.microsoft.com/en-us/kb/2999226
 Summary: Update for Universal C Runtime (CRT) in Windows. Before you install this update, check out the prerequisites section.
 Description: The Windows 10 Universal CRT is a Windows operating system component that enables CRT functionality on the Windows operating system. This update allows Windows desktop applications that depend on the Windows 10 Universal CRT release to run on earlier Windows operating systems.
  Microsoft Visual Studio 2015 creates a dependency on the Universal CRT when applications are built by using the Windows 10 Software Development Kit (SDK). You can install this update on earlier Windows operating systems to enable these applications to run correctly.
  This update applies to the following operating systems: Windows Server 2012 R2 Datacenter, Windows Server 2012 R2 Standard, Windows Server 2012 R2 Essentials, Windows Server 2012 R2 Foundation, Windows 8.1 Enterprise, Windows 8.1 Pro, Windows 8.1, Windows RT 8.1, Windows Server 2012 Datacenter, Windows Server 2012 Standard, Windows Server 2012 Essentials, Windows Server 2012 Foundation, Windows 8 Enterprise, Windows 8 Pro, Windows 8, Windows RT, Windows Server 2008 R2 Service Pack 1, Windows 7 Service Pack 1, Windows Server 2008 Service Pack 2, Windows Vista Service Pack 2
 Release Notes: * 1.0.20181019: Ensure the correct Install-WindowsUpdate function is called (work around [Boxstarter issue](https://github.com/chocolatey/boxstarter/issues/293))
  * 1.0.20170509: Fixed KB id in install script
  * 1.0.20170422: Changed to use chocolatey-windowsupdate.extension
  * 1.0.20161201: Fixed installation for windows 10 versions
  * 1.0.20161030: Fixed installation on a few specific OS versions; added checking for requisite Service Packs; simplified install script

KB3033929 1.0.5
 Title: Microsoft security advisory: Availability of SHA-2 code signing support for Windows 7 and Windows Server 2008 R2: March 10, 2015 | Published: 06/07/2020
 Number of Downloads: n/a | Downloads for this version: n/a
 Package url
 Chocolatey Package Source: https://github.com/jberezanski/ChocolateyPackages/tree/master/KB3033929
 Tags: microsoft windows update kb3033929 admin visual studio 2017
 Software Site: https://support.microsoft.com/en-us/kb/3033929
 Software License: https://support.microsoft.com/en-us/kb/3033929
 Summary: The update adds SHA-2 hashing algorithm signing and verification support to Windows 7 and Windows Server 2008 R2
 Description: Microsoft has released a security advisory for IT professionals about SHA-2 code signing support. The security advisory announces the availability of a security update and contains additional security-related information. To view the security advisory, go to the following Microsoft website:
  https://technet.microsoft.com/en-us/library/security/3033929

  This package requires Service Pack 1 for Windows 7 or Windows Server 2008 R2 to be installed first. The [KB976932 package](https://chocolatey.org/packages/KB976932) may be used to install it.

  On systems other than Windows 7 or Windows Server 2008 R2, this package installs successfully, but does nothing.

  This update is required to install Visual Studio 2017 on Windows 7 or Windows Server 2008 R2.
 Release Notes: 1.0.5: update dependencies to fix exit code handling on licensed Chocolatey.

KB3035131 1.0.3
 Title: Security update for Windows kernel: March 10, 2015 | Published: 06/07/2020
 Number of Downloads: n/a | Downloads for this version: n/a
 Package url
 Chocolatey Package Source: https://github.com/jberezanski/ChocolateyPackages/tree/master/KB3035131
 Tags: microsoft windows update kb3035131 admin
 Software Site: https://support.microsoft.com/en-us/kb/3035131
 Software License: http://go.microsoft.com/fwlink/?LinkID=206977
 Summary: This security update resolves privately reported vulnerabilities in Windows.
 Description: This security update resolves privately reported vulnerabilities in Windows. The most severe effect of the vulnerabilities is elevation of privilege if an attacker logs on to an affected system and runs a specially crafted application. An attacker who has successfully exploited the vulnerabilities could run arbitrary code in the security context of the account of another user who is logged on to the affected system. An attacker could then install programs; view, change, or delete data; or create new accounts that have full user rights. To learn more about the vulnerabilities, see [Microsoft Security Bulletin MS15-025](https://technet.microsoft.com/library/security/ms15-025).

  This package requires Service Pack 1 for Windows 7 or Windows Server 2008 R2 to be installed first. The [KB976932 package](https://chocolatey.org/packages/KB976932) may be used to install it. On Windows Vista or Windows Server 2008, Service Pack 2 is required.

  On systems later than Windows 8.1 or Windows Server 2012 R2, this package installs successfully, but does nothing.

  This update is recommended to install before installing KB3033929 on Windows 7 or Windows Server 2008 R2.
 Release Notes: 1.0.3: update extension dependency to fix exit code handling on licensed Chocolatey.

python 3.8.3
 Title: Python | Published: 06/07/2020
 Number of Downloads: n/a | Downloads for this version: n/a
 Package url
 Chocolatey Package Source: https://github.com/chocolatey-community/chocolatey-coreteampackages/tree/master/automatic/python
 Tags: python programming development foss cross-platform admin
 Software Site: http://www.python.org/
 Software License: https://www.python.org/download/releases/3.4.0/license
 Software Source: https://www.python.org/downloads/source
 Summary: Python is a programming language that lets you work more quickly and integrate your systems more effectively. You can learn to use Python and see almost immediate gains in productivity and lower maintenance costs.
 Description: Python is a programming language that lets you work more quickly and integrate your systems more effectively. You can learn to use Python and see almost immediate gains in productivity and lower maintenance costs.

  ## Notes

  - This package depends on the the latest major version of python package.

python3 3.8.3
 Title: Python 3.x | Published: 06/07/2020
 Number of Downloads: n/a | Downloads for this version: n/a
 Package url
 Chocolatey Package Source: https://github.com/chocolatey-community/chocolatey-coreteampackages/tree/master/automatic/python3
 Tags: python programming development foss cross-platform admin
 Software Site: http://www.python.org/
 Software License: https://www.python.org/download/releases/3.4.0/license
 Software Source: https://www.python.org/downloads/source
 Summary: Python 3.x is a programming language that lets you work more quickly and integrate your systems more effectively. You can learn to use Python 3.x and see almost immediate gains in productivity and lower maintenance costs.
 Description: Python 3.x is a programming language that lets you work more quickly and integrate your systems more effectively. You can learn to use Python 3.x and see almost immediate gains in productivity and lower maintenance costs.

  ## Package Parameters

  - `/InstallDir` - Installation directory. **NOTE**: If you have pre-existing python3 installation, this parameter is ignored and existing python install location will be used

  Example: `choco install python3 --params "/InstallDir:C:\your\install\path"`

  ## Notes

  - Python package manager `pip` is installed by default, but you can also invoke it using command `py -m pip` which will use `pip3` and adequate version of python if you also have python2 installed and/or pip2 on the `PATH`. For more details see [Python on Windows FAQ](https://docs.python.org/3/faq/windows.html).
  - For complete list of silent install options see the [Installing Without UI](https://docs.python.org/3/using/windows.html#installing-without-ui) page.

vcredist140 14.26.28720.3
 Title: Microsoft Visual C++ Redistributable for Visual Studio 2015-2019 | Published: 06/07/2020
 Number of Downloads: n/a | Downloads for this version: n/a
 Package url
 Chocolatey Package Source: https://github.com/chocolatey-community/chocolatey-coreteampackages/tree/master/automatic/vcredist140
 Tags: microsoft visual c++ redistributable 140 2015 2017 2019 admin
 Software Site: https://visualstudio.microsoft.com/vs/
 Software License: https://visualstudio.microsoft.com/license-terms/mlt031619/
 Documentation: https://docs.microsoft.com/en-us/visualstudio/ide/getting-started-with-cpp-in-visual-studio
 Issues: https://developercommunity.visualstudio.com/
 Summary: Run-time components that are required to run C++ applications that are built by using Visual Studio 2015-2019
 Description: Microsoft Visual C++ Redistributable for Visual Studio 2015-2019 installs run-time components of Visual C++ libraries. These components are required to run C++ applications that are developed using Visual Studio 2015-2019 and link dynamically to Visual C++ libraries. The packages can be used to run such applications on a computer even if it does not have Visual Studio 2015-2019 installed. These packages also install run-time components of C Runtime (CRT), Standard C++, MFC, C++ AMP, and OpenMP libraries.

  The Visual C++ Redistributable for Visual Studio 2015-2019 consists of files vcruntime140.dll, msvcp140.dll, vcomp140.dll, vcamp140.dll, mfc140.dll and other.

  ## Notes

  [Supported Operating Systems](https://docs.microsoft.com/en-us/visualstudio/releases/2019/system-requirements#microsoft-visual-c-2015-2019-redistributable-system-requirements): Windows 10, Windows 8.1 / Server 2012 R2 (with KB2919355), Windows 8 / Windows Server 2012, Windows 7 SP1 (with KB3033929) / Server 2008 R2 SP1, Windows Vista SP2 / Server 2008 SP2, Windows XP SP3 / Windows Server 2003 SP2

  On some systems, if KB2999226 is installed as a dependency of this package, the computer may need to be restarted before the installation of this package will succeed.

vcredist2015 14.0.24215.20170201
 Title: Microsoft Visual C++ Redistributable for Visual Studio 2015 Update 3 (with hotfix 2016-09-14) | Published: 06/07/2020
 Number of Downloads: n/a | Downloads for this version: n/a
 Package url
 Chocolatey Package Source: https://github.com/chocolatey/chocolatey-coreteampackages/tree/master/manual/vcredist2015
 Tags: microsoft visual c++ redistributable 2015 studio admin
 Software Site: https://blogs.msdn.microsoft.com/vcblog/2016/06/28/visual-studio-2015-update-3-available-now/
 Software License: https://msdn.microsoft.com/cc300389.aspx
 Documentation: https://msdn.microsoft.com/library/jj620919.aspx
 Issues: https://visualstudio.uservoice.com/forums/121579-visual-studio
 Summary: Run-time components that are required to run C++ applications that are built by using Visual Studio 2015 Update 3
 Description: Microsoft Visual C++ Redistributable for Visual Studio 2015 Update 3 installs run-time components of Visual C++ libraries. These components are required to run C++ applications that are developed using Visual Studio 2015 Update 3 and link dynamically to Visual C++ libraries. The packages can be used to run such applications on a computer even if it does not have Visual Studio 2015 installed. These packages also install run-time components of C Runtime (CRT), Standard C++, MFC, C++ AMP, and OpenMP libraries.

  ## Notes

  [Supported Operating Systems](https://www.visualstudio.com/en-us/productinfo/vs2015-sysrequirements-vs): Windows 10, Windows 8.1 / Server 2012 R2 (with KB2919355), Windows 8 / Windows Server 2012, Windows 7 SP1 (with KB3033929) / Server 2008 R2 SP1, Windows Vista SP2 / Server 2008 SP2, Windows XP SP3 / Windows Server 2003 SP2

  On some systems, if KB2999226 is installed as a dependency of this package, the computer may need to be restarted before the installation of this package will succeed.
 Release Notes: #### Program
  [Visual Studio 2015 Update 3 Release Notes](https://www.visualstudio.com/en-us/news/releasenotes/vs2015-update3-vs)
  #### Package
  14.0.24215.20170201: updated metadata, refactored into a virtual package dependent upon vcredist140

visualstudio-installer 2.0.1
 Title: Visual Studio Installer (remove only) | Published: 06/07/2020
 Number of Downloads: n/a | Downloads for this version: n/a
 Package url
 Chocolatey Package Source: https://github.com/jberezanski/ChocolateyPackages/tree/master/visualstudio-installer
 Tags: microsoft visual studio visualstudio vs 2017 2019 installer admin
 Software Site: https://visualstudio.microsoft.com/
 Software License: https://visualstudio.microsoft.com/license-terms/
 Documentation: https://docs.microsoft.com/en-us/visualstudio/ide/
 Issues: https://developercommunity.visualstudio.com/
 Summary: Provides a way to uninstall the Visual Studio Installer.
 Description: Uninstalling this package removes the Visual Studio Installer, which remains present after the Visual Studio product packages (such as [visualstudio2017enterprise](https://chocolatey.org/packages/visualstudio2017enterprise) or [visualstudio2019buildtools](https://chocolatey.org/packages/visualstudio2019buildtools)) are uninstalled.

  This package does not install anything by itself.
 Release Notes: ##### Software
  [Visual Studio 2019 release notes](https://docs.microsoft.com/en-us/visualstudio/releases/2019/release-notes)
  [Visual Studio 2017 release notes](https://docs.microsoft.com/en-us/visualstudio/releasenotes/vs2017-relnotes)
  ##### Package
  2.0.1: Fixed uninstall script.

visualstudio2017buildtools 15.9.23.0
 Title: Visual Studio 2017 Build Tools | Published: 06/07/2020
 Number of Downloads: n/a | Downloads for this version: n/a
 Package url
 Chocolatey Package Source: https://github.com/jberezanski/ChocolateyPackages/tree/master/visualstudio2017buildtools
 Tags: microsoft visual studio visualstudio vs 2017 build tools admin
 Software Site: https://visualstudio.microsoft.com/vs/
 Software License: https://www.visualstudio.com/license-terms/mlt687465/
 Documentation: https://docs.microsoft.com/en-us/visualstudio/welcome-to-visual-studio
 Issues: https://developercommunity.visualstudio.com/
 Summary: Build Tools allow you to build native and managed MSBuild-based applications without requiring the Visual Studio IDE.
 Description: ### Overview
  These Build Tools allow you to build native and managed MSBuild-based applications without requiring the Visual Studio IDE. There are options to install the Visual C++ compilers and libraries, MFC, ATL, and C++/CLI support.

  To find out what's new or to see the known issues, see the [Visual Studio 2017 Release Notes](https://www.visualstudio.com/en-us/news/releasenotes/vs2017-relnotes).
  [System requirements](https://www.visualstudio.com/en-us/productinfo/vs2017-system-requirements-vs)

  ### Customizations and Optional features
  By default, the package installs only the bare minimum required (the MSBuild Tools workload). The easiest way to add more development features is to use the workload packages listed in the Release Notes section.

  All package parameters are passed to the Visual Studio installer, enabling full customization of the installation. The possible parameters are [described here](https://docs.microsoft.com/en-us/visualstudio/install/use-command-line-parameters-to-install-visual-studio?view=vs-2017). The package passes `--norestart --wait` by default, and `--quiet`, unless `--passive` is specified in the package parameters.

  After installing the package, more features can also be added by launching the Visual Studio Installer application from the Start Menu.

  The language of the installed software can be controlled using the package parameter `--locale language`.
  The list of languages is [presented here](https://docs.microsoft.com/en-us/visualstudio/install/use-command-line-parameters-to-install-visual-studio?view=vs-2017#list-of-language-locales). By default, the operating system display language is used.

  #### Full installation
  This command will install Visual Studio Build Tools with all available workloads and optional components, display progress during the installation and specify the English language regardless of operating system settings:

  choco install visualstudio2017buildtools --package-parameters "--allWorkloads --includeRecommended --includeOptional --passive --locale en-US"

  [More package parameter examples](https://github.com/jberezanski/ChocolateyPackages/blob/master/chocolatey-visualstudio.extension/EXAMPLES.md)

  ### Notes

  A reboot may be required after (or even _before_) installing/uninstalling this package.
  If control over reboots is required, it is advisable to install the dependencies (esp. dotnet4.6.2 or later) first, perform a reboot if necessary, and then install this package.
 Release Notes: ##### Software
  [Visual Studio 2017 release notes](https://www.visualstudio.com/en-us/news/releasenotes/vs2017-relnotes)
  ##### Package
  15.9.23.0:
  - Package metadata updated for Visual Studio 2017 version 15.9.23 (the native installer always installs the latest released Visual Studio 2017 build).
  - Updated bootstrapper url.
  - Updated dependency version.

  #### Available workload packages
  - [Azure development build tools](https://chocolatey.org/packages/visualstudio2017-workload-azurebuildtools)
  - [Data storage and processing build tools](https://chocolatey.org/packages/visualstudio2017-workload-databuildtools)
  - [.NET desktop build tools](https://chocolatey.org/packages/visualstudio2017-workload-manageddesktopbuildtools)
  - [.NET Core build tools](https://chocolatey.org/packages/visualstudio2017-workload-netcorebuildtools)
  - [Node.js build tools](https://chocolatey.org/packages/visualstudio2017-workload-nodebuildtools)
  - [Office/SharePoint build tools](https://chocolatey.org/packages/visualstudio2017-workload-officebuildtools)
  - [Universal Windows Platform build tools](https://chocolatey.org/packages/visualstudio2017-workload-universalbuildtools)
  - [Visual C++ build tools](https://chocolatey.org/packages/visualstudio2017-workload-vctools)
  - [Visual Studio extension development build tools](https://chocolatey.org/packages/visualstudio2017-workload-visualstudioextensionbuildtools)
  - [Web development build tools](https://chocolatey.org/packages/visualstudio2017-workload-webbuildtools)
  - [Mobile Development with .NET](https://chocolatey.org/packages/visualstudio2017-workload-xamarinbuildtools)
  
# 3 - Partage de fichiers
* Sur PowerShell : 
    `New-Item "C:\SharedFolder" -itemType Directory`
    `New-SmbShare -Name SharedFolder1 -Path "C:\SharedFolder" -FullAccess "Tout le monde"`

* Dans notre vm :
    `yum install -y cifs-utils`
    `mkdir C:\SharedFolder`
    `mount -t cifs -o username=[...],password=[...] //192.168.1.28/SharedFolder /SharedFile`
    [root@localhost SharedFile]# `touch test.txt`
    [root@localhost SharedFile]# `ls`
    Output : 
        test.txt

* Sur PowerShell :
    PS C:\SharedFolder> `ls`
    Output : 
        Répertoire : C:\SharedFolder
        Mode                 LastWriteTime         Length Name
        ----                 -------------         ------ ----
        -a----        02/11/2020     18:42          0     test.txt