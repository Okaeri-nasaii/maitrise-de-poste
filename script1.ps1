#ce script affiche un résumé de l'os, liste les utilisateurs de la machine, affiche le ping moyen vers 8.8.8.8 et montre la bande passante du pc sur le réseau
$rep_time = (Test-Connection -ComputerName "8.8.8.8" -Count 4  | measure-Object -Property ResponseTime -Average).average
$criteria = "Type='software' and IsAssigned=1 and IsHidden=0 and IsInstalled=0"#checkupdate
$searcher = (New-Object -COM Microsoft.Update.Session).CreateUpdateSearcher()#
$updates = $searcher.Search($criteria).Updates#
Write-Output "-----------------------------"
Write-Output "FootPrint info"
Write-Output "-----------------------------"
$pcname = $env:computername
Write-Output "Le nom du PC est : $pcname"
$env:HostIP = ( ` Get-NetIPConfiguration | ` #on choppe l'ip
    Where-Object { `
            $_.IPv4DefaultGateway -ne $null `
            -and `
            $_.NetAdapter.Status -ne "Disconnected" `
    } `
).IPv4Address.IPAddress
$ip = $env:HostIP #on l'affiche
Write-Output "ip principale : $ip"
$osName = $(Get-WmiObject Win32_OperatingSystem).Caption
$osVer = $(Get-WmiObject Win32_OperatingSystem).Version
Write-Output "OS et sa version : $osName $osVer"
$startTime = $(gcim Win32_OperatingSystem).LastBootUpTime
Write-Output "date et heure de l'allumage : $startTime"

if ($updates.Count -ne 0) {
    $osUpdated = "le systeme n'est pas a jour :/"
}
else {
    $osUpdated = "le systeme est a jour :)"
}
Write-Output "est-ce que l'os est a jour : $osUpdated"

$Taille_RAM_MAX = [STRING]((Get-WmiObject -Class Win32_ComputerSystem ).TotalPhysicalMemory / 1GB)
$Taille_RAM_LIBRE = [String]((Get-WmiObject -Class Win32_OperatingSystem).FreePhysicalMemory / 1MB)
$Taille_RAM_UTILISE = [STRING]($Taille_RAM_MAX - $Taille_RAM_LIBRE) + " GB"
$ramLibre = systeminfo | select-object -index 27
$ramLibre = $ramLibre.Split(' ').Trim() -join ''
$ramLibre = $ramLibre.Split(':').Trim() -join ' '
Write-Output "Memoire virtuelle utilisee $Taille_RAM_UTILISE"
Write-Output "$ramLibre"
$TotalSpace = [Math]::Round((Get-Volume -DriveLetter 'C').Size / 1GB)
$FreeSpace = [Math]::Round((Get-Volume -DriveLetter 'C').SizeRemaining / 1GB)
$UsedSpace = ($TotalSpace - $FreeSpace)
Write-Output "Espace disque dispo $FreeSpace GB"
Write-Output "Espace disque utilise $usedSpace GB"
Write-Output "-----------------------------"
Write-Output "Net info"
Write-Output "-----------------------------"
Write-Output "le ping vers 8.8.8.8 est de $rep_time ms" #NB : si vous êtes dans les locaux d'YNOV, le ping vers 8.8.8.8 est impossible. Vous pouvez remplacer par 10.33.3.253 (cette IP n'est joignable QUE depuis le réseau d'YNOV)

$startTime = get-date #celui la est bricolé par bibi mais par la suite j'ai trouvé un truc plus simple que j'utilise pour l'upload test
$endTime = $startTime.AddSeconds(10)
$timeSpan = new-timespan $startTime $endTime
$count = 0

while ($timeSpan -gt 0){
   $colInterfaces = Get-WmiObject -class Win32_PerfFormattedData_Tcpip_NetworkInterface |select BytesTotalPersec
   foreach ($interface in $colInterfaces) {
      $bitsPerSec += $interface.BytesTotalPersec /8
      $count++
   }
   Start-Sleep -Seconds 1
   # recalculate the remaining time
   $timeSpan = new-timespan $(Get-Date) $endTime
}
$MoyDownload=($bitsPerSec/$count)/1000
Write-Output "download speed average within 10s : $MoyDownload Mb/s"


$DownloadURL = "https://bintray.com/ookla/download/download_file?file_path=ookla-speedtest-1.0.0-win64.zip"
$DownloadLocation = "$($Env:ProgramData)\SpeedtestCLI"
try {
    $TestDownloadLocation = Test-Path $DownloadLocation
    if (!$TestDownloadLocation) {
        new-item $DownloadLocation -ItemType Directory -force
        Invoke-WebRequest -Uri $DownloadURL -OutFile "$($DownloadLocation)\speedtest.zip"
        Expand-Archive "$($DownloadLocation)\speedtest.zip" -DestinationPath $DownloadLocation -Force
    }
}
catch {
    write-host "The download and extraction of SpeedtestCLI failed. Error: $($_.Exception.Message)"
    exit 1
}
$SpeedtestResults = & "$($DownloadLocation)\speedtest.exe" --format=json --accept-license --accept-gdpr
$SpeedtestResults = $SpeedtestResults | ConvertFrom-Json
$UploadSpeed = [math]::Round($SpeedtestResults.upload.bandwidth / 1000000 * 8, 2)

Write-Output "upload speed : $UploadSpeed Mb/s"
```

**Output :**

```
PS F:\Ywork> f:\Ywork\b1workstation\tp1\script\script.ps1